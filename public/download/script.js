const releaseDate = '2020-03-01T00:00-08:00';


const countdownTimer = function(end, update, interval) {
    const endDate = new Date(end);
    setInterval(function() {
        update(endDate - new Date());
    }, interval);
};


$(document).ready(function() {
    countdownTimer(releaseDate, function(ms) {
        const days = Math.trunc(ms / 86400000);
        ms %= 86400000;
        const hours = Math.trunc(ms / 3600000);
        ms %= 3600000;
        const minutes = Math.trunc(ms / 60000);
        ms %= 60000;
        const seconds = Math.trunc(ms / 1000);
        $('#countdown-days').text(days);
        $('#countdown-hours').text(hours.toString().padStart(2, "0"));
        $('#countdown-minutes').text(minutes.toString().padStart(2, "0"));
        $('#countdown-seconds').text(seconds.toString().padStart(2, "0"));
    }, 50);
});
