const bannerImageWidth = 2736;
const bannerImageHeight = 561;
let bannerFull = false;


const resize = function() {
    {  // banner resizing
        const windowWidth = $(window).width();
        if (windowWidth > bannerImageWidth * 300 / bannerImageHeight) {  // todo: 300 might not be right
            if (!bannerFull) {
                $('.banner').height('');
                $('.banner').width('100%');
                bannerFull = true;
            }
        } else {
            bannerFull = false;
            const bannerWidth = $('.banner').width();
            $('.banner').height(300);
            $('.banner').width('');
            // console.log($('.banner').css('height'));
        }
    }
    {  // banner horizontal placement
        const windowWidth = $(window).width();
        const bannerWidth = $('.banner').width();
        $('.banner').css('margin-left', `${(windowWidth - bannerWidth) / 2}px`);
    }
    {  // logo vertical placement
        const bannerHeight = $('.banner').height();
        const logoHeight = $('.logo').height();
        const topMargin = (bannerHeight + logoHeight) / 2;
        $('.logo').css('margin-top', `-${topMargin}px`);
        $('.logo').css('margin-bottom', `${topMargin - logoHeight}px`);
    }
    {  // nav item horizontal placement
        const windowWidth = $(window).width();
        const downloadPos = windowWidth / 2 - $('#download').outerWidth() / 2;
        const vr1Pos = downloadPos - $('#vr1').outerWidth() / 2;
        const homePos = vr1Pos - $('#home').outerWidth();
        const vr2Pos = downloadPos + $('#download').outerWidth() - $('#vr2').outerWidth() / 2;
        const galleryPos = vr2Pos + $('#vr2').outerWidth();
        $('#home').css('left', homePos);
        $('#vr1').css('left', vr1Pos);
        $('#download').css('left', downloadPos);
        $('#vr2').css('left', vr2Pos);
        $('#gallery').css('left', galleryPos);
    }
};

$(window).on('load', function() {
    $(window).scrollLeft(0);
    resize();
    $('.banner').show();
});


$(window).resize(resize);
